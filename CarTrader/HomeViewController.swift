//
//  ViewController.swift
//  CarTrader
//
//  Created by Naqash Khalid on 18/10/2016.
//  Copyright © 2016 Naqash Khalid. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var usedCar: UIButton!
    @IBOutlet weak var valuationBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
       self.usedCar.layer.cornerRadius = 20
        self.valuationBtn.layer.cornerRadius = 20
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension UIView {
    func ShadowView (radius : CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = false;
        
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 0.5;
        
        self.layer.contentsScale = UIScreen.main.scale;
        self.layer.shadowColor = UIColor.black.cgColor;
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 1.0;
        self.layer.shadowOpacity = 0.5;
        self.layer.masksToBounds = false;
        self.clipsToBounds = false;
    }
}
