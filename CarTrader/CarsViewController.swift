//
//  CarsViewController.swift
//  CarTrader
//
//  Created by Naqash Khalid on 19/10/2016.
//  Copyright © 2016 Naqash Khalid. All rights reserved.
//

import UIKit

class CarsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var cars = [UIImage.init(named: "car1"),UIImage.init(named: "car2"),UIImage.init(named: "car3"),UIImage.init(named: "car4")]
    var carName = [("Hino Briska","Auto 59 ks"),("1917 TGE-A","Manual 36 ks"),("Hino Renault 4CV","Auto 55 ks"),("Hino Briska XS","Auto 53 ks")]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
       
        // Do any additional setup after loading the view.
    }
  
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
        
    }
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cars", for: indexPath as IndexPath)
        cell.imageView?.image = self.cars[indexPath.row]
        var (carName,carSpeed) = self.carName[indexPath.row]
        cell.textLabel?.text = carName
        cell.detailTextLabel?.text = carSpeed
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    

}
