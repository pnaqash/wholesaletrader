//
//  ContactTableViewCell.swift
//  CarTrader
//
//  Created by Naqash Khalid on 20/10/2016.
//  Copyright © 2016 Naqash Khalid. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var callDuration: UILabel!
    @IBOutlet weak var contactName: UILabel!
    override func awakeFromNib() {
   
        super.awakeFromNib()
        
            }
//    override func layoutSubviews() {
//        contactImage.layer.cornerRadius = contactImage.bounds.height / 2
//        contactImage.clipsToBounds = true
//    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
