//
//  ContactsViewController.swift
//  CarTrader
//
//  Created by Naqash Khalid on 20/10/2016.
//  Copyright © 2016 Naqash Khalid. All rights reserved.
//

import UIKit

class ContactsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendEmail: UIButton!
    
    var contactImage = [UIImage.init(named: "dp1"),UIImage.init(named: "dp2"),UIImage.init(named: "dp3"),UIImage.init(named: "dp4")]
    var contactName = [("Janice Armstrong","Call : 08-10 am"),("Steavejob","Call : 12-10 pm"),("Brittnay Baker","Call : 03-05 am"),("Jeff Dilhinton","Call : 08-09 pm")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.ShadowView(radius: 5)
        self.tableView.tableFooterView = UIView()
        sendEmail.layer.cornerRadius = 30
        if (tableView.contentSize.height < tableView.frame.size.height) {
            tableView.isScrollEnabled = false
        }
        else {
            tableView.isScrollEnabled = true
        }
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactName.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contact", for: indexPath) as! ContactTableViewCell
//        cell.contactImage.layer.borderWidth = 1.0
//        cell.contactImage.layer.masksToBounds = false
//        cell.contactImage.layer.borderColor = UIColor.white.cgColor
//        cell.contactImage.layer.cornerRadius = cell.contactImage.frame.size.width/2
//        cell.contactImage.clipsToBounds = true
        let cellImageLayer: CALayer?  = cell.contactImage.layer
        cellImageLayer!.cornerRadius = 35
        cellImageLayer!.masksToBounds = true
        cell.contactImage.image = self.contactImage[indexPath.row]
        var (Name,callTime) = self.contactName[indexPath.row]
        cell.contactName?.text = Name
        cell.callDuration?.text = callTime
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    

}
